#include <nbtree/exn.hpp>

namespace rbox {
  Exn::Exn(const std::string &m) {
    msg = m;
  }
  Exn::~Exn() = default;
  const std::string &Exn::what() {
    return msg;
  }
  NullDeref::NullDeref() : Exn("Null Dereference") {
  }
}

namespace nbtree {
  NotFound::NotFound() : Exn("Element Not Found") {
  }
}
