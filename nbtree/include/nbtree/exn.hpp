#ifndef NBTREE_EXN

#include <string>

namespace rbox {
  class Exn {
    std::string msg;
  public:
    Exn(const std::string &m);
    virtual ~Exn();
    virtual const std::string &what();
  };
  class NullDeref : public Exn {
  public:
    NullDeref();
  };
}
namespace nbtree {
  class NotFound : public rbox::Exn {
  public:
    NotFound();
  };
}

#define NBTREE_EXN
#endif
