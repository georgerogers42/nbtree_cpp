#ifndef NBTREE_RBOX

#include <nbtree/exn.hpp>

namespace rbox {
  template<typename T>
  class Box {
    T *value = 0;
  public:
    typedef T value_type;
    Box() = default;
    Box(T *v) : Box() {
      value = v;
    }
    Box(const Box &o) : Box() {
      if(o) {
        value = new T(*o);
      } 
    }
    Box(Box &&o) : Box() {
      value = o.value;
      o.value = 0;
    }
    Box &operator=(T *v) {
      if(value) {
        delete value;
      }
      value = v;
      return *this;
    }
    Box &operator=(const Box &o) {
      auto xv = value;
      if(o) {
        value = new T(*o);
      } else {
        value = 0;
      }
      if(xv) {
        delete xv;
      }
      return *this;
    }
    Box &operator=(Box &&o) {
      if(value != o.value) {
        if(value) {
          delete value;
        }
        value = o.value;
      }
      o.value = 0;
      return *this;
    }
    ~Box() {
      if(value) {
        delete value;
      }
    }
    const T* operator->() const {
      if(!value) {
        throw NullDeref();
      }
      return value;
    }
    T* operator->() {
      if(!value) {
        throw NullDeref();
      }
      return value;
    }
    const T& operator *() const {
      if(!value) {
        throw NullDeref();
      }
      return *value;
    }
    T& operator *() {
      if(!value) {
        throw NullDeref();
      }
      return *value;
    }
    operator bool() const {
      return value;
    }
    bool operator==(const Box &o) const {
      return value == o.value;
    }
  };
}

#define NBTREE_RBOX
#endif
