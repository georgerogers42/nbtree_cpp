#ifndef NBTREE_NBTREE

#include <vector>
#include <nbtree/exn.hpp>
#include <nbtree/rbox.hpp>

namespace nbtree {
  template<typename K, typename V>
  class NBTreeMap {
    struct NBTreeNode {
      std::pair<const K, V> pair;
      NBTreeMap l, r;
    };
    rbox::Box<NBTreeNode> node;
  public:
    typedef const std::pair<const K, V>* pvalue_type;
    typedef std::pair<const K, V> value_type;
    typedef const K key_type;
    typedef V mapped_type;
    int size() const {
      if(node) {
        return 1 + node.l->size() + node.r->size();
      } else {
        return 0;
      }
    }
    void update(const K &k, const V &v) {
      auto self = this;
      while(self->node) {
        if(k == self->node->pair.first) {
          self->node->pair.first = k;
          self->node->pair.second = v;
          return;
        } else if(k < self->node->pair.first) {
          self = &self->node->l;
        } else {
          self = &self->node->r;
        }
      }
      self->node = new NBTreeNode{{k, v}, {}, {}};
    }
    bool member(const K &k) const {
      auto self = this;
      while(self->node) {
        if(k == self->node->pair.first) {
          return true;
        } else if(k < self->node->pair.first) {
          self = &self->node->l;
        } else {
          self = &self->node->r;
        }
      }
      return false;
    }
    const V &lookup(const K &k) const {
      auto self = this;
      while(self->node) {
        if(k == self->node->pair.first) {
          return self->node->pair.second;
        } else if(k < self->node->pair.first) {
          self = &self->node->l;
        } else {
          self = &self->node->r;
        }
      }
      throw NotFound();
    }
    V &find(const K &k) {
      auto self = this;
      while(self->node) {
        if(k == self->node->pair.first) {
          return self->node->pair.second;
        } else if(k < self->node->pair.first) {
          self = &self->node->l;
        } else {
          self = &self->node->r;
        }
      }
      self->node = new NBTreeNode {{k, V()}, {}, {}};
      return self->node->pair.second;
    }
    V &operator[](const K &k) {
      return find(k);
    }
    void unto_pvec(std::vector<pvalue_type> &pairs) {
      if(node) {
        node->l.unto_pvec(pairs);
        pairs.push_back(&node->pair);
        node->r.unto_pvec(pairs);
      }
    }
    void unto_vec(std::vector<value_type> &pairs) const {
      if(node) {
        node->l.unto_vec(pairs);
        pairs.push_back(node->pair);
        node->r.unto_vec(pairs);
      }
    }
  };
}

#define NBTREE_NBTREE
#endif
