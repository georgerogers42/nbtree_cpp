#ifndef ALPH_LIB

#include <string>
#include <regex>
#include <vector>
#include <fmt/ostream.h>
#include <nbtree/nbtree.hpp>

namespace alph {
  typedef unsigned long Count;
  typedef nbtree::NBTreeMap<std::string, Count> Table;

  typedef bool (*Order)(Table::pvalue_type a, Table::pvalue_type b);

  template<typename T>
  bool by_first(T a, T b) {
    return a->first < b->first;
  }

  template<typename T>
  bool by_second(T a, T b) {
    return a->second < b->second;
  }

  void capitalize(std::string &word);

  template<typename T>
  void build_table(T &m, std::istream &f) {
    using namespace std;
    static regex wordsplit(R"(\W+)");
    while(f) {
      string line;
      getline(f, line);
      sregex_token_iterator begin(line.begin(), line.end(), wordsplit, -1), end;
      for(auto i = begin; i != end; ++i) {
        string word = *i;
        capitalize(word);
        if(word != "") {
          ++m[word];
        }
      }
    }
  }

  template<typename T, typename O>
  void report(const std::ostream &w, T &m, O o) {
    using namespace std;
    vector<typename T::pvalue_type> pairs;
    m.unto_pvec(pairs);
    sort(pairs.begin(), pairs.end(), o);
    for(auto pair : pairs) {
      fmt::print("{:>24}: {:>10}\n", pair->first, pair->second);
    }
  }
}

#define ALPH_LIB
#endif
