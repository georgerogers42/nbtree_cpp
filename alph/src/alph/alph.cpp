#include <alph/alph.hpp>

namespace alph {
  void capitalize(std::string &word) {
    if(word == "") {
      return;
    }
    word[0] = toupper(word[0]);
  }
}
