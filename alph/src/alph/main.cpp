#include <regex>
#include <string>
#include <iostream>
#include <fstream>
#include <alph/alph.hpp>
#include <nbtree/nbtree.hpp>
#include <fmt/ostream.h>

using namespace std;
using namespace nbtree;
using namespace alph;

typedef unsigned long Count;
typedef NBTreeMap<string, Count> Table;
typedef bool (*Order)(Table::pvalue_type a, Table::pvalue_type b);

int main(int argc, char **argv) {
  vector<string> args;
  for(int i = 1; i < argc; ++i) {
    args.push_back(argv[i]);
  }
  Order cmp = by_second;
  if(args.size() > 0 && args[0] == "-a") {
    cmp = by_first;
  }
  for(int i = 1; i <= 100;i++) {
    fmt::print("# Begin Iteration {}\n", i);
    ifstream f("TEXT-PCE-127.txt");
    if(!f) {
      fmt::print(cerr, "File Not Found\n");
      exit(1);
    }
    Table m;
    build_table(m, f);
    report(cout, m, cmp);
    fmt::print("# End Iteration {}\n", i);
  }
  return 0;
}
